from utils import ascii_list as ad

class Node():
    def __init__(self, label, code):
        self.label = label
        self.code = code
        self.childs = []

    def add_child(self, node):
        if type(node) == self.__class__:
            self.childs.append(node)
        else: 
            print("si può aggiungere solo un oggetto di tipo nodo!")


class LzwCompressor():

    def __init__(self):
        self.input_file = []
        
      
        #si fa uso di un trie creato come una lista di nodi, 
        #ai quali si possono aggiungere figli
        self.trie = []

    
    def __search(self, sx):
        lbl = sx[0]
        for node in self.trie:
            if lbl == node.label and len(sx) > 1:
                code = self.__search_node(sx, 1, node)
                return code
            elif lbl == node.label:
                return node.code
                
    def __search_node(self, sx, i, node):
        if len(sx) == i:
            return node.code
        for child in node.childs:
             #in questo caso sto verificando se la label del figlio
             #è uguale al carattere iesimo della stringa
            if child.label == sx[i]:
                return self.__search_node(sx, i+1, child)
        self.c += 1
        q = Node(sx[i], self.c)
        node.add_child(q)
        return None

    def __find(self, code): 
        for node in self.trie:
            if code == node.code:
                node_f = self.__find_node(code, node)
                return node_f
            
            
                
    def __find_node(self, code, node):
        if code == node.code:
            return node
        for child in node.childs:
            if child.code == code:
                return self.__find_node(code, child)
        self.c += 1
        q = Node(chr(code), self.c)
        node.add_child(q)
        return None


    def compress(self, file_path):
        self.input_file.clear()
        #contatore per aggiungere nuovi codici, parte dall'ultimo carattere del
        # codice ascii
        self.c = 256
        with open(file_path) as f:
            for lines in f.readlines():
                for c in lines:
                    self.input_file.append(c)
        #pulisco il trie per maggiore sicurezza e consistenza
        self.trie.clear()
        alfabeto_file = ad.create_list(self.input_file)
        ltmp = [(chr(i),i) for i in range(0,256)]
        #ottimizzazione per mantenere piccolo il trie in fase di compressione
        # il trie in questo caso viene composto solo dai caratteri interni al file
        for i in alfabeto_file:
            for j,code in ltmp:
                if i == j:
                    node = Node(i, code)
                    self.trie.append(node)   

        name = input("inserire il nome del file compresso: ")
        fout = open(name+'.Z', "w")
        end_char = 256
        s = self.input_file[0]
        print(self.input_file)
        j = 1
        while j < len(self.input_file):
            x  = self.input_file[j]
            p = self.__search(s+x)
            if p != None:
                s += x
                j += 1
                continue
            else:
                ris = self.__search(s)
                fout.write(str(ris)+' ')
                s = x
                j += 1
        fout.write(str(end_char)+' ')
        fout.close()

    def decompress(self, file_path):
        #contatore per aggiungere nuovi codici, parte dall'ultimo carattere del
        # codice ascii
        self.c = 256
        self.input_file.clear()
        with open(file_path) as f:
            for lines in f.readlines():
                for c in lines.split(' '):
                    self.input_file.append(c)
        #pulisco il trie per maggiore sicurezza e consistenza
        self.trie.clear()
        #in questo caso costruisco l'intero trie sul codice ascii,
        #anche se potrei ottimizzare prendendo solo i caratteri il cui codice è inferiore
        #al valore 256
        ltmp = [(chr(i), i) for i in range(0,256)]
        for label,code in ltmp:
            node = Node(label, code)
            self.trie.append(node)
        name = input("come vuoi chiamare il file decompresso?")
        fdcomp = open(name+'.txt', "w")
        end_char = 256
     
        print(self.input_file)
        j = 0
        while j < len(self.input_file):
            p = self.__find(self.input_file[j])
            if p != None:
                j+=1
                while self.__find_node(self.input_file[j], p) == None:
                    j += 1
                    continue
            else:
                #devo usare quello al passo precendente, perchè 
                #ho già incrementato il contatore
                ris = self.__find(self.input_file[j-1])
                fdcomp.write(str(ris))
                
        fdcomp.close()




a = LzwCompressor()

a.compress("prova.txt")
a.decompress("abc.Z")
