# -*- coding: utf-8 -*-

from huffman import HuffmanUtils
from huffman.filereader import filereader
from math import ceil

"""
Module that implements a Huffman compressor
Created on Fri Oct  5 11:48:13 2018

@author: Mauro Leoncini
"""

def compress(fn):
    '''
    V 0.1. A esclusiva valenza didattica per il corso di Linguaggi dinamici.
    Prima costruisce una stringa che "rappresenta" la stringa compressa, ma
    con caratteri al posto dei bit. In questo modo è più facile
    per lo studente "ispezionare" i vari passaggi della compressione.
    Solo dopo trasforma la stringa in sequenza binaria e la scrive su file
    '''
    file = filereader(fn)
    frequencies = file.frequencies
    huffmanTree = HuffmanUtils.tree(frequencies)
    codewordsDict = HuffmanUtils.code(huffmanTree)
    characters, header = HuffmanUtils.serialize(huffmanTree)
    compressedString = ''
    for char in file:
        compressedString += codewordsDict[char]
    padding = (8 - len(header+compressedString) % 8) % 8
    content = bytearray()
    content.append(padding)
    characters = bytearray("".join(characters), "utf-8")
    content.append(len(characters))
    content += characters
    content += bitstring_to_bytes(header+compressedString)
    with open(fn+'.hc', "wb") as f:   # hc stands for huffman code
        f.write(content)

def decompress(compressedFile):
    '''v0.1 Decomprime un file compresso con Huffman.compress(). Valgono
       le stesse considerazioni fatte per compress'''
    filename, extension = compressedFile.rsplit('.',1)
    padding,characters,string = from_disk(compressedFile)
    huffman_tree, content = HuffmanUtils.rebuild_tree(string, characters)
    s = ''
    i = -1
    current_node = huffman_tree
    while i<len(content)-max(padding,1): # Se padding è 0 si va fuori dagli indici
        if current_node.internal():
            i += 1
            if content[i] == '0':
                current_node = current_node.left
            else:
                current_node = current_node.right
        else:
            s += current_node
            current_node = huffman_tree
    with open(filename,'w') as g:
        for c in s:
            g.write(c)

def bitstring_to_bytes(s):
    '''Trasforma una stringa di caratteri '0'/'1' in una sequenza di bit, usando
    il tipo di dato bytes (interi nell'intervallo [0,255])'''
    s = [s[i:i+8] for i in range(0, len(s), 8)]
    s.reverse()
    s = "".join(s)
    v = int(s, 2)
    b = bytearray()
    for i in range(0,int(len(s)/8)):
        b.append(v & 0xff)
        v >>= 8
    r = len(s)%8
    if r > 0:
        v <<= 8-r
        b.append(v & 0xff)
    #return bytes(b[::-1])
    return bytes(b)

def from_disk(filename):
    '''Legge il file compresso e restiuisce una terna composta da: (1) padding,
    (2) lista dei caratteri distinti presenti nel file sorgente, ordinati nel
    modo in cui devono comparire nelle foglie dell'Huffman tree, da sinistra a  
    destra, (3) la sequenza di "caratteri" '0'/'1' che codifica la struttura
    dell'Huffman tree e (a seguire, senza necessità di separazione) il file 
    compresso vero e proprio'''
    def nextchar(f):
        code = ord(f.read(1))
        if code<128:
            return chr(code),1
        code2 = ord(f.read(1))
        return bytearray((code,code2)).decode('utf-8'),2
    
    with open(filename, "rb") as f:
        padding = int.from_bytes(f.read(1), 'big')
        nchars = int.from_bytes(f.read(1), 'big')
        #characters = [ chr(c) for c in bytearray(f.read(num_chars)) ]
        characters = []
        while nchars>0:
            c,l = nextchar(f)
            characters.append(c)
            nchars -= l
        s = ''
        while True:
            b = f.read(1)
            if not b:
                break
            c = '{0:08b}'.format(int.from_bytes(b, 'big'))
            s += c
    return padding,characters,s    
