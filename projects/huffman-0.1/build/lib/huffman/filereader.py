#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 13 00:20:32 2018

@author: mauro
"""

import codecs

class NotSupportedEncoding(Exception):
    pass

class filereader:
    '''Iteratore sui caratteri del file passato come parametro in fase di 
       inizializzazione. Fornisce anche tramite due proprietà, l'indicazione
       della codifica del file e un dizonario con le frequeze dei caratteri.'''
    def __init__(self, fn):
        self.filename = fn

    def __encoding(self):
        '''Prova a leggere il file usando differenti codifiche'''
        if not self._encoding:
            for code in ['ascii', 'utf-8', 'latin-1']:
                try:
                    with (codecs.open(self.filename, 'r', encoding=code)) as file:
                        file.readlines()
                except UnicodeDecodeError:
                    pass
                else:
                    self._encoding = code
                    break
            if not self._encoding:
                raise NotSupportedEncoding
        return self._encoding

    def __iter__(self):
        def next(x):
            with (codecs.open(x.filename, 'r', encoding=x.encoding)) as f:
                for c in f.read():
                    yield c
        return(next(self))

    def __frequencies(self):
        '''Calcola e memorizza le frequenze dei caratteri'''
        if not self._frequencies:
            for c in self:
                self._frequencies[c] = self._frequencies.get(c, 0) + 1
        return self._frequencies

    def __getattr__(self, name):
        '''Viene chiamata quando si cerca di accedere ad un attributo che non
           esiste (o non esiste ancora). Gli attributi _encoding e _frequencies
           vengono inizializzati; per tutti gli altri si produce un errore.'''
        if name == '_frequencies':
            self.__dict__[name] = {}
        elif name == '_encoding':
            self.__dict__[name] = None
        else:
            raise AttributeError
        return self.__dict__[name]
    
    def __setattr__(self, name, value):
        '''Non si permette l'inserimento di altri attributi'''
        if name in ['filename', '_encoding','_frequencies']:
            super().__setattr__(name, value)
        else:
            raise AttributeError

    @property
    def encoding(self):
        return self.__encoding()

    @property
    def frequencies(self):
        return self.__frequencies()
