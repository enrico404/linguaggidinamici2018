#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 12 23:07:52 2018

@author: mauro

Insieme di funzioni di utilità per manipolare alberi che
rappresentano codici Huffman
"""

from huffman.priority_queue import priority_queue

class node(str):
    '''Classe che definisce i nodi di un Huffman tree. I nodi ereditano dalle
       stringhe, il che significa che sono a tutti gli effetti stringhe (più
       precisamente, singoli caratteri) ma con gli attributi che caratterizzano
       gli Huffman tree: un attributo priorità, significativo solo per le
       foglie, e attributi left e right che "puntano" rispettivamente ai 
       sotto-alberi di sinistra e di destra'''
       
    def __new__(cls, symb='', priority=0, left=None, right=None):
        c = super().__new__(cls, symb)
        c.__priority = priority
        c.__left = left
        c.__right = right
        return c

    def __lt__(self, other):
        return self.priority < other.priority

    def internal(self):
        return self == ''

    @property
    def priority(self):
        return self.__priority

    @property
    def left(self):
        return self.__left

    @property
    def right(self):
        return self.__right

def tree(frequencies):
    '''Dato un dizionario con le frequenze restituisce un
    Huffman tree i cui nodi sono elementi della classe node'''
    if len(frequencies) == 1:
        if list(frequencies.keys())[0] == '0':
            frequencies['1'] = 0
        else:
            frequencies['0'] = 0
    queue = priority_queue()
    for key, value in frequencies.items():
        queue.insert(node(symb=key, priority=value))
    for _ in range(queue.size()-1):
        min1 = queue.minimum()
        min2 = queue.minimum()
        new_node = node(priority=min1.priority+min2.priority, \
                        left=min1, right=min2)
        queue.insert(new_node)
    return queue.minimum()

def code(tree):
    '''Dato un Huffman tree restituisce il dizionario che definisce
    il codice, cioè l'associazione fra simboli e caratteri,
    utilizzabile per la codifica'''
    def visit(tree, hcode):
        if tree is None:
            return
        yield from visit(tree.left, hcode+'0') if tree.left is not None else ()
        if tree != '': # '' characterizes internal nodes
            yield tree, hcode
        yield from visit(tree.right, hcode+'1') if tree.right is not None else ()
    code = {}
    for inode, codeword in visit(tree, ''):
        code[inode] = codeword
    return code

def serialize(huffman_tree):
    '''Dato un Huffman tree restituisce la coppia formata dai simboli
    presenti nelle foglie del tree (ordinati da sinistra verso destra) e 
    dalla stringa di 0/1 che rappresenta la struttura dell'albero.'''
    if not huffman_tree.internal():
        return [huffman_tree],'1'
    ltree, lcode = serialize(huffman_tree.left)
    rtree, rcode = serialize(huffman_tree.right)
    return ltree+rtree, '0'+lcode+'0'+rcode

def rebuild_tree(code, char_list):
    '''E' la funzione inversa di serialize.'''
    if not char_list:
        return
    flag = code[0]
    code = code[1:]
    if flag == '1':
        leaf = node(symb=char_list.pop(0))
        return leaf,code
    leftchild,code = rebuild_tree(code,char_list)
    code = code[1:]
    rightchild,code = rebuild_tree(code,char_list)
    return node(left = leftchild, right = rightchild),code 
