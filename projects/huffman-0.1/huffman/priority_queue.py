#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 13 00:05:45 2018

@author: mauro
"""

class EmptyPriorityQueue(Exception):
    pass

class priority_queue:
    '''Implementa una coda con priorità (min-based) di oggetti arbitrari, che
       devono avere la sola proprietà di essere confrontabili con l'operatore <.
       L'attuale implementazione usa una semplice lista non ordinata. Questo
       rende efficiente l'inserzione ma non la ricerca e l'estrazione
       dell'oggetto con minimo valore di priorità'''
       
    def __init__(self, L=[]):
        if not L:
            self.__list = []
        else:
            self.__list = L

    def insert(self, obj):
        self.__list.append(obj)

    def is_empty(self):
        return self.__list == []

    def size(self):
        return len(self.__list)

    def minimum(self):
        if self.is_empty():
            raise EmptyPriorityQueue
        minindex = 0
        for i in range(1, len(self.__list)):
            if self.__list[i] < self.__list[minindex]:
                minindex = i
        x = self.__list[minindex]
        del self.__list[minindex]
        return x