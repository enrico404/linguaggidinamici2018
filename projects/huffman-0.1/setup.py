#!/usr/bin/env python3

from distutils.core import setup

setup(name='huffman',
      version='0.1',
      description='Simple Huffman compressor and decompressor',
      author='mauro Leoncini',
      author_email='leoncini@unimore.it',
      url='https://github.com/Linguaggi-Dinamici-2018-Modulo-Python/',
      license='MIT',
      packages=['huffman'],
     )
