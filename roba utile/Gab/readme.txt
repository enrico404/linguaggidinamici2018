Progetto Linguaggi Dinamici - generazione orario scolastico

Gabriele Bani, matr 91434


Breve descrizione:
	- L'applicazione web consente ad un amministratore di inserire dati riguardanti professori e corsi, per poi poter generare un orario che rispetti determinati vincoli. 
	- I docenti possono visualizzare i loro orari dopo aver eseguito il login
	- gli utenti anonimi possono visualizzare gli orari generati



Requisiti per l'utilizzo:
	- python3.4
	- django
	- numpy
	- pulp (per la programmazione lineare)
	



Come utilizzare l'applicazione:
	- per poter inserire dati, e' necessario essere amministratori. Attualmente l'unico amministratore e' admin2, con password adad1234.
	- le password dei docenti sono composte dalle iniziali di nome e cognome ripetute due volte seguite da 1234. Esempio, per l'utente marco.prato avremo password mpmp1234
	- e' presente un dump del database chiamato db.json.
	- la navigazione e' molto semplice, e si limita per la maggior parte alla visualizzazione degli orari.


