def f(x):
    return 2*x


if __name__ == "__main__":
    lista = ['a', 'b', 'c']
    lista2 = [i for i in range(1,20)]
    l2 = [(lambda i: 2*i)(i) for i in range(0,10) if i>5]
   # l2 = [f(i) for i in range(0, 10)]

    for i in l2:
        print(i)