class Animale():
    def __init__(self):
        self.tipo = "Animale"
        
    def verso(self):
        pass
    

class Cane(Animale):
    def __init__(self):
        self.tipo = "Cane"
    def verso(self):
        print("bau bau \n")
    

class Gatto(Animale):
    def __init__(self):
        self.tipo = "Gatto"
    def verso(self):
        print("Miao miao \n")


if __name__ == "__main__":
    a = Animale()
    c = Cane()
    g = Gatto()
    c.verso()
    g.verso()
    print("polimorfismo \n")
    a = g
    a.verso()