#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

import os     
if __name__=="__main__":
    path = os.getcwd().split('/')
    if path[len(path)-1] == "Desktop" or path[len(path)-1] == "Scrivania":
        os.system("cd linguaggidinamici2018; git pull; cd Jupyter-notebooks; git pull ")
        os.system("cd GAVI; git pull")
        os.system("cd reti; git pull")
        os.system("cd calcoloparallelo2018; git pull; cd cp18; git pull")
    else:
        print("the current directory must be the Desktop \n")