import sys
import math
def lg(x):
        if x == 0:
            return 1
        else: 
            return (math.floor(math.log(x, 2))+1)

f1 = open("binFile.Z", 'wb')

array = [10,20,30,40,50, 260]



for i in array:
    nbytes = math.ceil(lg(i)/8)
    print(nbytes)
    f1.write(i.to_bytes(nbytes, 'big'))
f1.close()


with open("binFile.Z", 'rb') as f:
    data = f.read()
    print(data)
    print(data[5:7])
    a= int.from_bytes(data[1:2], 'big')
    print(a)