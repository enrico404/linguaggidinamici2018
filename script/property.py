class temp:
    def __init__(self, tmp):
        self.__temperature = tmp
    def set_temp(self, tmp):
        self.__temperature = tmp
        print("into set temp")
    def get_temp(self):
        print("into get temp")
        return self.__temperature
    temp = property(get_temp, set_temp)

if __name__=="__main__":
    t = temp(10)
    print("temperature: ", t.temp)
    t.temp = 15
    print("temperature: ", t.temp)