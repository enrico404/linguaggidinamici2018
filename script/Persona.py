class Persona:

   #metodo inizializatore
    def __init__(self, nome, cognome, tel, cf):
        #attributi
        self.nome  = nome 
        self.cognome = cognome
        self.tel = tel
        self.cf = cf
    #metodo getter del telefono---
    def getTel(self):
        return self.tel
    
    def getNome(self):
        return self.nome

    def getCognome(self):
        return self.cognome

    def setTel(self, numero):
        self.tel = numero

    def setName(self, name):
        self.nome = name


if __name__ == "__main__":
    soggetto = Persona("pippa", "aaaa", 1111, "abcd")
    print(soggetto.getNome())

    soggetto.setName("luca")
    print("print dopo cambio nome")
    print(soggetto.getNome())
