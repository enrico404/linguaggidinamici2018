import json
from functools import reduce

def lsum(x,y):
    return x+y



def squadre(partita):
    return [(partita['team1']['name'],partita['team1']['code']),\
           (partita['team2']['name'],partita['team2']['code'])]

def stampa(lista):
    for item in lista:
        print(item)



fp = open('datasets/serie_A/2015-16.json')
campionato = json.load(fp)
partite = campionato['rounds'][0]['matches']
#print(json.dumps(p, indent=2))
#squadre partecipanti al campionato
squadre_partecipanti = []
# for p in partite:
#     squadre_partecipanti.append((p['team1']['name'],p['team1']['code']))
#     squadre_partecipanti.append((p['team2']['name'],p['team2']['code']))
#
# for s in squadre_partecipanti:
#     print(s)

l = reduce(lambda x,y: x+y, [squadre(p) for p in partite]) #pythonic
# print(l)


#list comprehension
# [i for i in range(1,11)]  crea un lista di numeri
partita = campionato['rounds'][0]['matches'][0]
# print(partita)
lista = []
# for i in range(1, 381):
#     if(i > (381/2)):
#         lista.append((i ,campionato['name'],campionato['rounds'][0]['name'],'ritorno',squadre(partita)[0][1],squadre(partita)[1][1]))
#     else:
#         lista.append((i, campionato['name'], campionato['rounds'][0]['name'], 'andata', squadre(partita)[0][1],
#                       squadre(partita)[1][1]))

count = 0
#conto il numero di partite
for i in campionato['rounds']:
    for j in i['matches']:
        count += 1


i = 0
for partite in campionato['rounds']:
    for p in partite['matches']:
            i += 1
            if( i < count/2):
                lista.append((i, campionato['name'], partite['name'], 'andata', squadre(p)[0][0], squadre(p)[1][0]))
            else:
                lista.append((i, campionato['name'], partite['name'], 'ritorno', squadre(p)[0][0], squadre(p)[1][0]))


stampa(lista)
