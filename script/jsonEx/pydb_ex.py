import pymysql
import json
from functools import reduce


db = pymysql.connect("localhost", "user", "user", "testdb")
cursor = db.cursor()

query = "drop table if exists squadre"
cursor.execute(query)
query = "create table squadre(id INT(11) NOT NULL AUTO_INCREMENT, " +\
                              "nome VARCHAR(16) NOT NULL, " +\
                              "PRIMARY KEY ( id ));"
cursor.execute(query)
squadre = list(enumerate(reduce(lambda x,y: x+y,[(p['team1']['name'],p['team2']['name']) for p in campionato['rounds'][0]['matches']]), 1))
query = "INSERT INTO squadre VALUES (%s, %s);"

try:
    cursor.executemany(query, squadre)
    db.commit()
except:
    print("doing rollback...")
    db.rollback()
