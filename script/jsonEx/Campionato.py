#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-


import pymysql
import json
from functools import reduce
import os

class campionato:
    def __init__(self, campionato_or_filename=None):
        """campionato è una stringa del tipo Seria A 2015/16"""
        if not campionato_or_filename:
            return
        with open(campionato_or_filename) as f:
            campionato = json.load(f)
        nomecamp = campionato['name']
        cursor = self.__dbOpen()
        query = "SELECT ID FROM Campionati WHERE nome=%s;"
        cursor.execute(query, nomecamp)
        results = cursor.fetchone()
        self.__idSQ = {}
        if not results:
            cursor.close()
            self.__nuovoCamp(campionato_or_filename)

    def getIdSq(self):
        cursor = self.__dbOpen()
        query = "select id, nome from Squadre"

        try:
            cursor.execute(query)
            results = cursor
            cursor.close()
            for res in results:

                self.__idSQ[res[1]] = res[0]
        except:
            # error
            cursor.close()
            return -1

    def __dbOpen(self):
        '''
        Apre una connessione con il database di test
        '''
        return pymysql.connect("localhost", "user", "user", "testdb").cursor()

    def execQueryIns(self, query, params):
        cursor = self.__dbOpen()
        try:
            cursor.execute(query, params)
            cursor.connection.commit()
        except:
            cursor.connection.rollback()
            return False
        cursor.close()
        return True

    def __nuovoCamp(self, name):
        with open(name) as f:
            campionato = json.load(f)
        nomecamp = campionato['name']
        cursor = self.__dbOpen()
        query = "INSERT into Campionati (nome) VALUES (%s)"
        if self.execQueryIns(query, nomecamp) == False:
            print("errore durante inserimento campionato")
        giornate = campionato['rounds']
        prima = [(partita['team1']['name'], partita['team2']['name']) \
                 for partita in giornate[0]['matches']]
        squadre = reduce(lambda x, y: x+y, prima)
        query = "select id from Squadre where nome= %s;"
        query2 = "insert into Squadre (nome) values(%s);"
        for squadra in squadre:
            results = cursor.execute(query, squadra)
            if not results:
                if self.execQueryIns(query2, squadra) == False:
                    print("errore durante inserimento squadre")
        # compilo dizionario per le squadre
        self.getIdSq()
        query3 = "INSERT INTO Calendario " + \
                "(Campionato, giornata, AR, data, locali, ospiti) " + \
                "VALUES (%s, %s, %s, %s, %s, %s);"
        query4 = "SELECT id FROM Calendario WHERE " + \
                 "locali = %s AND ospiti = %s AND data = %s;"
        query5 = "INSERT INTO Risultati " + \
                 "(partita, retiLocali, retiOspiti) " + \
                 "VALUES (%s, %s, %s);"

        idCampQ = "select id from Campionati where nome ='"+nomecamp+"'"
        results =cursor.execute(idCampQ)
        idCamp = results
        numsquadre = len(squadre)
        durata = numsquadre - 1
        for numGiornata, giornata in enumerate(giornate):
            for partita in giornata['matches']:
                params = (idCamp, numGiornata%durata+1, \
                           ['A','R'][numGiornata>=durata], \
                           partita['date'], \
                           self.__idSQ[partita['team1']['name']],
                           self.__idSQ[partita['team2']['name']])

                params2 = (self.__idSQ[partita['team1']['name']],
                           self.__idSQ[partita['team2']['name']], partita['date'])
                if self.execQueryIns(query3, params) == False:
                    print("Errore durante inserimento Calendario")
                results = cursor.execute(query4, params2)
                idRis = cursor.fetchone()
                try:
                    params3 = (idRis, partita['score1'], partita['score2'])
                    cursor.execute(query5, params3)
                    cursor.connection.commit()
                except:
                    cursor.connection.rollback()

        queryView = "create view ris as select cal.id, c.nome as campionato, cal.giornata, cal.AR, cal.data, s1.nome as sq1, s2.nome as sq2, r.retiLocali, r.retiOspiti " +\
                "from Campionati c, Calendario cal, Squadre s1, Squadre s2, Risultati r " +\
                    "where c.id = cal.campionato and cal.locali=s1.id and cal.ospiti = s2.id and r.id = cal.id order by id asc;"

        cursor.execute(queryView)

        cursor.close()



    def partita(self, locali, ospiti):
        """restituisce risultati di una partita """
        cursor = self.__dbOpen()
        lid = self.__idSQ[locali]
        oid = self.__idSQ[ospiti]
        query = "SELECT retiLocali, retiOspiti " +\
            "FROM Risultati R, Calendario C " +\
            "WHERE  R.id = C.id and locali='"+str(lid)+"' and ospiti='"+str(oid)+"';"
        try:
            res = cursor.execute(query)
            out = cursor.fetchone()
            cursor.close()
            return (locali, ospiti, out[0], out[1])
        except:
            print("query error")
            return -1

    def doppioConfronto(self, sq1, sq2):
        '''
               Restituisce i dati del doppio confronto fra due squadre;
               prima il risultato dell'andata e poi quello del ritorno,
               indipendentemente dall'ordine con cui sono state specificate
               le due squadre
               '''
        p1 = self.partita(sq1, sq2)
        p2 = self.partita(sq2, sq1)

        return (p1,p2)

    def cammino(self,sq):
        '''
                Restituisce tutti i risultati (in ordine di giornata)
                di una data squadra
                '''
        cursor = self.__dbOpen()
        query = "select * "+\
                "from ris "+\
                "Where sq1 = %s or sq2 = %s order by data asc"

        try:
            res = cursor.execute(query, (sq, sq))
            out = cursor.fetchall()
            cursor.close()
            return (out)
        except:
            print("query error")
            return -1

    def giornata(self, num, girone):
        '''
              Restituisce i risultati di una giornata, specificata
              col numero e con l'indicazione di andata (A) o ritorno (R)
              '''
        cursor = self.__dbOpen()
        query = "select * "+\
                "from ris "+\
                "Where AR='"+girone+"' and giornata='"+str(num)+"';"

        try:
            res = cursor.execute(query)
            out = cursor.fetchall()
            cursor.close()
            return (out)
        except:
            print("query error")
            return -1






os.system("sudo mysql -p < testdb.sql")
c  = campionato('./datasets/serie_A/2016-17.json')
print("ris: ", c.partita("Roma", "Juventus"))
print("doppio confronto", c.doppioConfronto("Roma", "Juventus"))
print("cammino della Juvemtus: ")
tuples = c.cammino("Juventus")
for t in tuples:
    print(t)

print("giornata 1, girone A:---------" )
tuples = c.giornata(1, "A")
for t in tuples:
    print(t)
