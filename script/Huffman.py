import codecs

class filereader:

    def __init__(self, filename):
        self.filename = filename
        self.__frequencies = dict()
        self.__text = ''

    def __reader(self):
        if not self.__text:
            with codecs.open(self.filename, 'r', encoding='latin1') as f:
                self.lines = f.readline()
            self.__text = ''
            for line in self.lines:
                self.__text += line

    def __set_frequencies(self):
        if not self.__frequencies:
            self.__reader()
            for c in self.__text:
                # if c not in list(self.frequencies.keys()):
                #     self.frequencies[c] = 1
                # else:
                #     self.frequencies[c] += 1
                self.__frequencies[c] = self.__frequencies.get(c, 0) + 1
            return self.__frequencies

    def __iter__(self):

        self.__reader()
        self.index = 0
        self.length = (len(self.__text))
        return self

    def __next__(self):
        if self.index == self.length:
            raise StopIteration
        c = self.__text[self.index]
        self.index += 1
        return c

    @property
    def frequencies(self):
        return self.__set_frequencies()


class priority_queue:

    def __init__(self):
        self.__list = []

    def __lt__(self):
        pass

    def is_empty(self):
        return self.__list == []

    def insert(self, item):
        self.__list.append(item)

    def minimum(self):
        i = 1
        minimo = 0
        while i<len(self.__list):
            if self.__list[i]< self.__list[minimo]:
                minimo = i
                i += 1
        a = self.__list[minimo]
        self.__list.pop(minimo)
        return a


