def function():
    r = range(10, 20, 2)
    print(r.start)


def listaIter():
    """ documentation string example """
    L = ['cioa','prova']
    iterator = iter(L)
    while True:
        try:
            statement = next(iterator)
            print("ris: "+ statement)
        except StopIteration:
            break

class OutOfLimit(Exception):
    pass




def exceptionExample():

    v=0
    while True:
        try:
            v = input("Scegli un numero intero fra 1 e 10: ")
            n = int(v)
            if (n < 1 or n > 10):
                #OutOfLimit è un errore creato da noi(eredita da Exception) non esite in python
                raise OutOfLimit
            break
        except ValueError:
            print("Letterale non riconosciuto come numero intero! Riprovare ...")
        except OutOfLimit:
            print("Il numero non è compreso nell'intervallo desiderato! Riprovare ...")

    print("hai scelto il numero {}".format(v))


def data_type():
    print(type(3))
    print(type(3.14))
    print(type('Hello world'))
    print(type("Hello world"))
    print(type(True))
    print(type(('a', 1, 'b', 2)))
    print(type(['a', 1, 'b', 2]))
    print(type({'a': 1, 'b': 2}))
    print(type({'a', 'b', 'c'}))


def lambdaF():
    print(type(lambda x: x + 1))

def memoryLocation():
    x = 10
    print(id(x))
    x = 3.14
    print(id(x))


class counter:
    """attempt 1.0"""
    def __init__(self, start):
        self.val = start

    def getVal(self):
        return self.val

    def inc(self):
        self.val += 1

    def reset(self):
        self.val = 0

class Fibonacci:
    def __init__(self, last):
        self.last = last

    def __iter__(self):
        self.x = 0
        self.nextx = 1
        self.n = 1
        return self

    def __next__(self):
        val = self.x
        if self.n > self.last:
            raise StopIteration

        #swap esteso -->
        # tmp = self.nextx
        # self.nextx = self.x + self.nextx
        # self.x = tmp
        # swap compatto -->
        self.x, self.nextx = self.nextx, self.x + self.nextx
        self.n += 1
        return val

#
# class Fibonacci2:
#     """è più pulita la versione 1 però """
#
#   def __init__(self, last):
#         self.last = last
#         self.x = 0
#         self.nextx = 1
#         self.n = 1
#
#     def __iter__(self):
#         return self
#
#     def __next__(self):
#             val = self.x
#             if self.n > self.last:
#                 raise StopIteration
#             # swap esteso -->
#             # tmp = self.nextx
#             # self.nextx = self.x + self.nextx
#             # self.x = tmp
#             # swap compatto -->
#             self.x, self.nextx = self.nextx, self.x + self.nextx
#             self.n += 1
#             return val

class Fibonacci4:
    '''Iterarable that dynamically "construct" an iterator
       object for the Fibonacci numbers'''

    class Fibiter:
        pass

    def __init__(self, last):
        self.last = last

    def __iter__(self):
        fbi = Fibonacci4.Fibiter()
        fbi.x = 0
        fbi.nextx = 1
        fbi.n = 1
        fbi.last = self.last

        def next(self):
            '''Fibonacci-specific cursor method'''
            val = self.x
            if self.n > self.last:
                raise StopIteration
            self.x, self.nextx = self.nextx, self.x + self.nextx  # Pythonic
            self.n += 1
            return val

        Fibonacci4.Fibiter.__next__ = next
        # fbi.__next__ = next  non funzionerebbe perchè il complatore si aspeta di aver definito già il metodo next
        return fbi


class test:
    def __init__(self):
        self.__x = 0




if __name__ == '__main__':
    #listaIter()
    #print("doc string: "+ listaIter.__doc__)
    #exceptionExample()
    #data_type()
    #lambdaF()
    #memoryLocation()

    #c = counter(4)
    #c.inc()
    #print (c.getVal())

    #fib = Fibonacci4(10)

    # il for lo fa implicitamente es. for i in fib \n print i
    # fi = iter(fib)
    # i = 0
    # while i<10:
    #     print (fi.__next__())
    #     i = i+1

    # for i in fib:
    #     print(i)
    #non è n vero e proprio incapsulamento
    t = test()
    t._test__x = 10
    print(t._test__x)