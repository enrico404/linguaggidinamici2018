pack_top_level="Animali"  #nome package sotto forma di stringa

#azioni = ["parla", "si_muove", "mangia", "beve", "dorme"] 
azioni = ["parla", "mangia", "beve", "dorme"]   

#classe Cane
c1 = "Cane"
nomi1 = [ "Lassie", "Rocky", "Laika", "Lucky", "Tobia" ]
max_eta1 = 20 
caratteristiche1 = [ "pastore", "levriero", "maremmano", "schnauzer", "barboncino", "bassotto" ]
t1 = (nomi1, max_eta1, caratteristiche1)

#classe Cavallo
c2 = "Cavallo"
nomi2 = [ "Freccia", "Spirit", "Stella", "Strike", "Furia" ]
max_eta2 = 25
caratteristiche2 = [ "nero", "baio", "pezzato", "fulvo" ]
t2 = (nomi2, max_eta2, caratteristiche2)

#classe Leone
c3 = "Leone"
nomi3 = [ "Leo", "Leonida", "Leonzio", "Simba", "Lebi" ]
max_eta3 = 20
caratteristiche3 = range(150,225)
t3 = (nomi3, max_eta3, caratteristiche3)

#classi_attive = {c2:t2, c1:t1, c3:t3}
classi_attive = {c2:t2, c1:t1}

