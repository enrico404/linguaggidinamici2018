#!/usr/bin/python
# coding=utf8

# importiamo funzioni utili
from time import sleep
from random import randint


# importiamo le classi (nomi delle classi hard-coded nel codice)

from Animali.Cane import *
from Animali.Cavallo import *
from Animali.Leone import *


# lista oggetti da generare, inizialmente vuota
list_a = []

# generazione numero di oggetti per ogni sottoclasse
num_cani = randint(1,10) 
num_cavalli = randint(1,10) 
num_leoni = randint(1,10) 

# possibili nomi degli animali - lista di 5 nomi random
nomi_cani = [ "Lassie", "Rocky", "Laika", "Lucky", "Tobia" ]
nomi_cavalli = [ "Freccia", "Spirit", "Stella", "Strike", "Furia" ]
nomi_leoni = [ "Leo", "Leonida", "Leonzio", "Simba", "Lebi" ]

# eta' massime
max_eta_cane = 20
max_eta_cavallo = 25
max_eta_leone = 20

# possibili caratteristiche degli animali 
razze_cani = [ "pastore", "levriero", "maremmano", "schnauzer", "barboncino", "bassotto" ]
mantelli_cavalli = [ "nero", "baio", "pezzato", "fulvo" ]
pesi_leoni = range(150,225)


# crea i cani
for i in range(num_cani):
	list_a.append(
		Cane(
			nome = nomi_cani[randint(0,len(nomi_cani)-1)],
			eta = randint(1,max_eta_cane),
			razza = razze_cani[randint(0,len(razze_cani)-1)]
		)
	)

# crea i cavalli
for i in range(num_cavalli):
	list_a.append(
		Cavallo(
			nome = nomi_cavalli[randint(0,len(nomi_cavalli)-1)],
			eta = randint(1,max_eta_cavallo),
			mantello = mantelli_cavalli[randint(0,len(mantelli_cavalli)-1)]
		)
	)

# crea i leoni
for i in range(num_leoni):
	list_a.append(
		Leone(
			nome = nomi_leoni[randint(0,len(nomi_leoni)-1)],
			eta = randint(1,max_eta_leone),
			peso = pesi_leoni[randint(0,len(pesi_leoni)-1)]
		)
	)

# usiamo gli oggetti creati
for i in range(20):
	# scegliamo un oggetto a caso
	id_list_a = randint(0,len(list_a)-1)
	# scelta mantiene l'oggetto scelto
	scelta = list_a[id_list_a]
	# scegliamo una azione a caso: convenzione che numera le azioni da 0 a 4 
	id_azione = randint(0,4)
	
	# costruiamo la frase
	frase = scelta.nome()
	frase = frase + " (età "
	frase = frase + str(scelta.eta())
	frase = frase + " anni, "
	frase = frase + scelta.info()
	frase = frase + ") "

	# e effettuiamo l'azione - nomi delle azioni hard-coded nel codice
	if id_azione == 0:
		frase = frase + scelta.parla()
	elif id_azione == 1:
		frase = frase + scelta.si_muove()
	elif id_azione == 2:
		frase = frase + scelta.mangia()
	elif id_azione == 3:
		frase = frase + scelta.beve()
	elif id_azione == 4:
		periodo = scelta.dorme(randint(1,3))
		frase = frase + "dorme"

	print(frase)

	# sospensione per 1 secondo tra una iterazione e l'altra
	sleep(1)

