#!/usr/bin/python
# -*- coding: utf-8 -*-

from time import sleep

class Animale(object):

	# costruttore
	def __init__(self, nome, eta):
		self.__nome = nome
		self.__eta = eta

	# metodo get/set per nome
	def nome(self, nome = None):
		# se il nome e' definito
		if (nome != None):
			# lo assegna
			self.__nome = nome
		# in ogni caso lo ritorna
		return self.__nome

	# metodo get/set per eta'
	def eta(self, eta = None):
		# se l'eta' e' definita
		if (eta != None):
			# la assegna
			self.__eta = eta
		# in ogni caso la restituisce
		return self.__eta

	# metodo per dormire
	def dorme(self, secondi = 1):
		# di default dorme 1 secondo
		t = secondi
		# si sospende per t secondi
		sleep(t)
		return t

	# metodo beve - uguale per tutte le sottoclassi
	def beve(self):
		return "beve"

	# metodo mangia - uguale per 2 sottoclassi su 3 
	def mangia(self):
		return "mangia"



	

	
	

