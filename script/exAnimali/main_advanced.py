#!/usr/bin/python
# coding=utf8

# importiamo funzioni utili
from time import sleep
from random import randint
# importiamo le classi (nomi delle classi hard-coded nel codice)
from config import *
exec("from "+pack_top_level+" import *")

def creaAnimali(num, lista, anim, id):
    for i in range(num):
        string = anim + "("+ nomi[id][randint(0, len(nomi) - 1)] + "," + str(randint(1, max_eta[id])) + "," + str(caratteristiche[id][randint(0, len(caratteristiche[id]) - 1)]) + ")"
        lista.append(
            eval(string)
        )



# lista oggetti da generare, inizialmente vuota
lista = []

# generazione numero di oggetti per ogni sottoclasse
num_cani = randint(1,10)
num_cavalli = randint(1,10)
num_leoni = randint(1,10)


creaAnimali(num_cani,lista,"Cane",0)
creaAnimali(num_cavalli,lista,"Cavallo",1)
creaAnimali(num_leoni,lista,"Leone",2)
print(lista)


# usiamo gli oggetti creati
for i in range(20):
    # scegliamo un oggetto a caso
    id_list_a = randint(0, len(lista) - 1)
    # scelta mantiene l'oggetto scelto
    scelta = lista[id_list_a]
    # scegliamo una azione a caso: convenzione che numera le azioni da 0 a 4
    id_azione = randint(0, 4)

    # costruiamo la frase
    frase = scelta.nome()
    frase = frase + " (età "
    frase = frase + str(scelta.eta())
    frase = frase + " anni, "
    frase = frase + scelta.info()
    frase = frase + ") "

    # e effettuiamo l'azione - nomi delle azioni hard-coded nel codice
    if id_azione == 0:
        frase = frase + scelta.parla()
    elif id_azione == 1:
        frase = frase + scelta.si_muove()
    elif id_azione == 2:
        frase = frase + scelta.mangia()
    elif id_azione == 3:
        frase = frase + scelta.beve()
    elif id_azione == 4:
        periodo = scelta.dorme(randint(1, 3))
        frase = frase + "dorme"

    print(frase)

    # sospensione per 1 secondo tra una iterazione e l'altra
    sleep(1)

