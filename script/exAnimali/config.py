

pack_top_level="Animali"  #nome package sotto forma di stringa
classi = ["Cane", "Cavallo", "Leone"]
azioni = ["parla", "si_muove", "mangia", "beve", "dorme"]

# possibili nomi degli animali - lista di 5 nomi random per ogni classe
nomi = [ \
[ "Lassie", "Rocky", "Laika", "Lucky", "Tobia" ], \
[ "Freccia", "Spirit", "Stella", "Strike", "Furia" ], \
[ "Leo", "Leonida", "Leonzio", "Simba", "Lebi" ]\
]

# possibili caratteristiche degli animali - liste di lunghezza diversa 
caratteristiche = [[ "pastore", "levriero", "maremmano", "schnauzer", "barboncino", "bassotto" ], \
[ "nero", "baio", "pezzato", "fulvo" ], \
range(150,225) \
]

# eta' massime per Cane, Cavallo, Leone
max_eta=[20,25,20] 

