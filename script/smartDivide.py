def smart_divide(func):
    def inner(a,b):
        if b == 0:
            print("divisione non possibile")
            return
        return func(a,b)
    return inner

@smart_divide
def divide(a,b):
    return a/b


if __name__ == "__main__":
    a = int(input("inserire a:"))
    b = int(input("inserire b:"))
    print(divide(a,b))