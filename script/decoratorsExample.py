def dec(func):
    def inner(a):
        print("decoro la funzione")
        print(a)
        print(globals())

        return func

    return inner


@dec
def f(a):
    print("sono la funzione non decorata, passo il parametro a..")


if __name__ == "__main__":
    a = "hola chico"
    f(a)
