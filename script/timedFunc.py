from time import time


def my_timer(f):
    def timedfun(n):
        #time restuisce il tempo in secondi passati dal 1970
        start_time = int(round(time()*1000))
        res = f(n)
        stop_time = int(round(time()*1000))
        timedfun.time = stop_time-start_time
        return res
    return timedfun
#non usare i decoratori con le funzioni ricorsive, perchè ogni chiamata viene chiamata la funzione decorata e quindi aumenta
#esponenzialmente l'overhead
@my_timer
def Fibonacci(n):
    f0=0
    f1=1
    for _ in range(n-1):
        f0, f1 = f1, f0+f1
    return f1

def FibRic(n):
    if n==0 or n==1:
        return n
    return FibRic(n-1)+FibRic(n-2)

n = 100000
Fibonacci(n)
print("tempo impiegato da fibonacci: {}ms valore di n: {} ".format(Fibonacci.time, n))