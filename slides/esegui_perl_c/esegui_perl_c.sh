#Programma che esegue e misura il tempo di esecuzione dei comandi
#Output del comando time: 
#real (wall clock time tra l'invocazione e la terminazione) 
#user (user-mode code including libraries) 
#sys (kernel-mode)

echo Esecuzione di var1 variabili globali
time ./var1
echo Esecuzione di var2 variabili locali
time ./var2
echo Esecuzione di perl1 variabili globali
time perl perl1.pl
echo Esecuzione di perl2 variabili locali
time perl perl2.pl





