#import del modulo inspect (reflection) - mette a disposizione funzioni ulteriori per ispezionare il codice delle classi

import inspect

class Person:
	
	def getFirstname(self):
		return self.firstname
	

	def setFirstname(self,firstname):
		self.firstname = firstname
	

	def getLastname(self):
		return self.lastname
	

	def setLastname(self,lastname):
		self.lastname = lastname
	

	def getAge(self):
		return self.age
	

	def setAge(self,age):
		self.age = age
	

	def stampa(self):
		print("Classe Person")
	


def getClassName():

	#Do appropriate stuff here to find out the classname 
	#(eg. reading from a configuration file) -> name in a string
	return "Person";
	

if __name__ == "__main__":
	classname = getClassName()
	c1 = globals()[getClassName()]    #riferimento a classe
	obj1 = globals()[getClassName()]()   #riferimento a istanza 
	print("Stampa dei metodi della classe:")
	for i,j in c1.__dict__.items():
		if inspect.isfunction(j):
			print(i)
	print("Invocazione metodo che inizia con 'stampa':")
	for i,j in c1.__dict__.items():
		if i.startswith("stampa"):
			getattr(obj1, i)()


