import java.util.*;

public class GenericStack <T> {
   private ArrayList<T> stack = new ArrayList<T> ();
   private int top = -1;
   int size ;

   public GenericStack(int s){
	   size = s > 0 && s < 1000 ? s : 10 ;  
   }
 
   public boolean isEmpty() { return top == -1 ; } 
   public boolean isFull()  { return top == size - 1 ; } 

   public boolean push (T item) {
	if (!isFull())
	{
                stack.add (++top, item);
		return true ;  // push successful
	}
	return false ;  // push unsuccessful
   }
  
   public T pop () {
 	
	   return stack.remove (top--);
   }
 
   public static void main (String[] args) {
	  GenericStack<Integer> is = new GenericStack<Integer> (5);
          GenericStack<Double> ds = new GenericStack<Double> (8);


	  int n = 17;
          while (is.push(n)) {
		System.out.format ("%4d%n", n); 
		n += 1 ;
	  }
	  while (!is.isEmpty())  { 
          	n = is.pop ();
          	System.out.format ("%4d%n", n);
	  }

	  double x = 30.1;
          while (ds.push(x)) {
		System.out.format ("%4f%n", x); 
		x += 1.1 ;
	  }
	  while (!ds.isEmpty())  { 
          	x = ds.pop ();
          	System.out.format ("%4f%n", x);
	  }
   }
 
}

