public class Square implements Shape {
	private int x;
	private int y;
	private int width;

	public Square(int x, int y, int width) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
	}

	@Override
	public void draw() {
		System.out.println(
			"Drawing Square at (" + x + "," + y +
			") with width " + width);
	}
}
