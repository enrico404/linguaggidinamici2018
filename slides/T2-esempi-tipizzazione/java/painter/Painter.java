import java.util.*;

public class Painter {

	public static void main(String[] args) {
		// We would like the shapes to be drawn in the
		// order of the shape types as found in this list

		// The shapes
		List<Shape> shapes = new LinkedList<Shape>();
		shapes.add(new Circle(1,1,5));
		shapes.add(new Circle(3,3,7));
		shapes.add(new Square(2,4,3));
		shapes.add(new Square(4,2,4));

		for (Shape shape : shapes) {
			shape.draw();
		}
	}
}
