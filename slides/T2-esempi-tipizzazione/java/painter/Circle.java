public class Circle implements Shape {
	private int x;
	private int y;
	private int radius;

	public Circle(int x, int y, int radius) {
		super();
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	@Override
	public void draw() {
		System.out.println("Drawing Circle at (" +
					x + "," + y + ") with radius " + radius);
	}
}
