// The Shape interface declares the method draw()
public interface Shape {
	public void draw();
}
