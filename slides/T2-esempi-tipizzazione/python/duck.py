class Duck:
    def quack(self):
        print("Quaaaaaack!")
    def feathers(self):
        print("The duck has white and gray feathers.")
 
class Person:
    def quack(self):
        print("The person imitates a duck.")
    def feathers(self):
        print("The person takes a feather from the ground and shows it.")
    def name(self):
        print("John Smith")
 
def in_the_farm(x):
    x.quack()
    x.feathers()
    #x.name()  #produce errore su oggetto di classe Duck
 
def game():
    donald = Duck()
    john = Person()
    in_the_farm(donald)
    in_the_farm(john)
 
game()
