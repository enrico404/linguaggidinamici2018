class Stack:
     def __init__(self, size):
	self.size = size         
	self.items = []

     def isEmpty(self):
         return len(self.items) == 0

     def isFull(self):
	 return len(self.items) == self.size

     def push(self, item):
	 if not self.isFull():
	         self.items.append(item)
		 return 1
	 return 0

     def pop(self):
         return self.items.pop()

    
s=Stack(5)

x = 17
while s.push(x):
	print(x)
	x += 1

while not s.isEmpty():
	n=s.pop()
	print(n)

x=3.5
while s.push(x):
	print(x)
	x += 1.1

while not s.isEmpty():
	n=s.pop()
	print(n)



s.push(4)
s.push('dog')
s.push(True)
s.push("Ciao Ciao")
print(s.pop())
print(s.pop())

