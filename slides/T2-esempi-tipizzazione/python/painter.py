#!/usr/bin/python

# No common Shape interface
# Circle and Square are completely independent classes

class Circle():
	def __init__(self,x,y,radius): #costruttore. NOTA: parametri senza tipo indicato
		self.x = x
		self.y = y
		self.radius = radius
	def draw(self):
		print "Drawing Circle at (%d,%d) with radius %d" % (self.x, self.y,self.radius)

class Square():
	def __init__(self,x,y,width):
		self.x = x
		self.y = y
		self.width = width
        def draw(self):
		print "Drawing Square at (%d,%d) with width %d" % (self.x, self.y,self.width)

# If invoked as a script, executes the following code
if __name__ == "__main__":
	shapes = [Circle(1,1,5), Circle(3,3,7), Square(2,4,3), Square(4,2,4)]
	#shapes is a list of objects - NOTE: objects of different types
	for shape in shapes:   
		shape.draw()

#Note: no parenthesis 
